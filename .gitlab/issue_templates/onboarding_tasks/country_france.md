### For team members in France

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `1 to 3-month Probationary Period until YYYY-MM-DD (relevant months after hire date)` *See contract for exact time frame
   * Effective Date: `1-3 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `1-3 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.

</details>
