#### For Finance Only

<details>
<summary>Finance</summary>

1. [ ] Finance: Add to Comerica (as user or viewer only if in Finance).
1. [ ] Finance: Add *Employee Record* in NetSuite with department classification.
1. [ ] Finance: Add to Carta with department classification.
1. [ ] Finance: If the new team member is a Finance Business Partner or an [Executive](https://about.gitlab.com/company/team/structure/#executives), please notify @ewegscheider using the [Greenhouse Approvals Change Request](https://gitlab.com/gl-recruiting/operations/issues/new#) Issue template.

</details>
