#### For Support Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Open a new [Support Engineer onboarding issue](https://gitlab.com/gitlab-com/support/support-training/issues/new?issue) using the issue template for [Support Engineer onboarding](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20Support%20Engineer.md) and provide the link in a comment below this onboarding checklist.
1. [ ] Manager: If the new team member will be a ZenDesk Admin, make sure to include that in the access request. Make sure to also add them to the [GitHub Support org](https://github.com/GitLab-Support) which is used for mirroring the ZenDesk theme.

</details>
