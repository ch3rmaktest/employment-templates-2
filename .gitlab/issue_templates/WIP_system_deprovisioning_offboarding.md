### System Deprovisioning

#### System Admins

Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a System you administer access to:**
- Review and remove the former team member's access.
- Once access has been removed from the impacted system, select the checkbox indicating access has been deprovisioned and add a comment stating access has been deprovisioned.

**If the user DOES NOT have access to a system you administer access to:**
- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.
- Add a comment in this issue stating which application you have reviewed and confirmed no access.

| System | Deprovisioned |
| --- | --- |
| ADP | <ul><li>[ ] Yes </ul></li> |
| AWS (production) | <ul><li>[ ] Yes </ul></li> |
| AWS (support/staging) | <ul><li>[ ] Yes </ul></li> |
| AWS (gitter) | <ul><li>[ ] Yes </ul></li> |
| AWS (gov cloud) | <ul><li>[ ] Yes </ul></li> |
| AWS (gitlab.io) | <ul><li>[ ] Yes </ul></li> |
| Avalara | <ul><li>[ ] Yes </ul></li> |
| Azure | <ul><li>[ ] Yes </ul></li> |
| Bizible | <ul><li>[ ] Yes </ul></li> |
| Blackline | <ul><li>[ ] Yes </ul></li> |
| Calendly | <ul><li>[ ] Yes </ul></li> |
| Carta | <ul><li>[ ] Yes </ul></li> |
| Conga Contracts | <ul><li>[ ] Yes </ul></li> |
| ContactOut | <ul><li>[ ] Yes </ul></li> |
| ContractWorks | <ul><li>[ ] Yes </ul></li> |
| Cookiebot | <ul><li>[ ] Yes </ul></li> |
| Docusign | <ul><li>[ ] Yes </ul></li> |
| DoGood | <ul><li>[ ] Yes </ul></li> |
| Elastic Cloud | <ul><li>[ ] Yes </ul></li> |
| Eventbrite | <ul><li>[ ] Yes </ul></li> |
| Fastly CDN | <ul><li>[ ] Yes </ul></li> |
| FunnelCake | <ul><li>[ ] Yes </ul></li> |
| GitLab.com Prod/staging rails and db console (ssh) | <ul><li>[ ] Yes </ul></li> |
| Staging-GitLab | <ul><li>[ ] Yes </ul></li> |
| customers. -gitlab.com (ssh) | <ul><li>[ ] Yes </ul></li> |
| forum. -gitlab.com (ssh) | <ul><li>[ ] Yes </ul></li> |
| license.gitlab.com | <ul><li>[ ] Yes </ul></li> |
| license. -gitlab.com (ssh) | <ul><li>[ ] Yes </ul></li> |
| version. -gitlab.com (ssh) | <ul><li>[ ] Yes </ul></li> |
| ops.gitlab.net | <ul><li>[ ] Yes </ul></li> |
| dev.gitlab.org | <ul><li>[ ] Yes </ul></li> |
| Google Cloud Platform | <ul><li>[ ] Yes </ul></li> |
| Google Search Console | <ul><li>[ ] Yes </ul></li> |
| Google Tag Manager | <ul><li>[ ] Yes </ul></li> |
| GovWin IQ | <ul><li>[ ] Yes </ul></li> |
| Grafana (dashboards.gitlab.net) | <ul><li>[ ] Yes </ul></li> |
| Hello Sign | <ul><li>[ ] Yes </ul></li> |
| Lean Data | <ul><li>[ ] Yes </ul></li> |
| LicenseApp | <ul><li>[ ] Yes </ul></li> |
| LucidChart | <ul><li>[ ] Yes </ul></li> |
| Mandrill | <ul><li>[ ] Yes </ul></li> |
| MailGun | <ul><li>[ ] Yes </ul></li> |
| Meetup | <ul><li>[ ] Yes </ul></li> |
| Modern Health | <ul><li>[ ] Yes </ul></li> |
| Moo | <ul><li>[ ] Yes </ul></li> |
| Moz Pro | <ul><li>[ ] Yes </ul></li> |
| Mural | <ul><li>[ ] Yes </ul></li> |
| Optimal Workshop | <ul><li>[ ] Yes </ul></li> |
| PackageCloud | <ul><li>[ ] Yes </ul></li> |
| PathFactory | <ul><li>[ ] Yes </ul></li> |
| Periscope | <ul><li>[ ] Yes </ul></li> |
| Qualtrics | <ul><li>[ ] Yes </ul></li> |
| Rackspace  | <ul><li>[ ] Yes </ul></li> |
| Salesforce | <ul><li>[ ] Yes </ul></li> |
| Screaming Frog | <ul><li>[ ] Yes </ul></li> |
| Sertifi | <ul><li>[ ] Yes </ul></li> |
| Shopify | <ul><li>[ ] Yes </ul></li> |
| Sigstr | <ul><li>[ ] Yes </ul></li> |
| Snowflake | <ul><li>[ ] Yes </ul></li> |
| Snowplow | <ul><li>[ ] Yes </ul></li> |
| Spoke | <ul><li>[ ] Yes </ul></li> |
| Sprout Social | <ul><li>[ ] Yes </ul></li> |
| Status - IO | <ul><li>[ ] Yes </ul></li> |
| Stitch | <ul><li>[ ] Yes </ul></li> |
| Stripe | <ul><li>[ ] Yes </ul></li> |
| Tenable.IO | <ul><li>[ ] Yes </ul></li> |
| Tipalti | <ul><li>[ ] Yes </ul></li> |
| TheHive | <ul><li>[ ] Yes </ul></li> |
| Unbabel | <ul><li>[ ] Yes </ul></li> |
| Visual Compliance | <ul><li>[ ] Yes </ul></li> |
| WebEx | <ul><li>[ ] Yes </ul></li> |
| Will Learning | <ul><li>[ ] Yes </ul></li> |
| Xactly | <ul><li>[ ] Yes </ul></li> |
| YouTube | <ul><li>[ ] Yes </ul></li> |
| Zuora | <ul><li>[ ] Yes </ul></li> |

/confidential

/label  ~offboarding