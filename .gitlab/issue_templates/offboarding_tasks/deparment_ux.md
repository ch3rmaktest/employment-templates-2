### FOR UX RESEARCHERS ONLY

<summary>Manager</summary>

1. [ ] Manager: Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud)
1. [ ] Manager: Remove former team member from [SurveyMonkey](https://surveymonkey.net)
1. [ ] Manager: Remove former team member from [UsabilityHub](https://usabilityhub.com)
1. [ ] Manager: Remove former team member from [MailChimp](https://mailchimp.com/)
1. [ ] Manager: Remove former team member from the [GitLab Dribbble team](https://dribbble.com/gitlab).
1. [ ] Manager: Remove former team member's `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: Remove [SketchApp](http://www.sketchapp.com/) license via Sketch License Manager.

<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : remove team member from the `@uxers` User Group on Slack.