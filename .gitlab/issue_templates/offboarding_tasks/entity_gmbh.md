### For GitLab GmbH employees only

<summary>People Ops</summary>

1. [ ] People Ops: Inform payroll (@hdevlin) of last day of employment.

