This project folder is exclusively for managing the templates for the following issues:

- [Onboarding](.gitlab/issue_templates/onboarding.md)
- [Offboarding](.gitlab/issue_templates/offboarding.md)
- [Internal Transition](.gitlab/issue_templates/internal_transition.md)

The issues itself are created in the [Team Member Epics group](https://gitlab.com/gitlab-com/team-member-epics/)
